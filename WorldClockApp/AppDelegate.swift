//
//  AppDelegate.swift
//  WorldClockApp
//
//  Created by macbook on 19.04.21.
//

import UIKit
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        window = UIWindow(frame: UIScreen.main.bounds)
        let welcomVC = WelcomViewController()
        let navigationController = UINavigationController(rootViewController: welcomVC)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        return true
    }
}

