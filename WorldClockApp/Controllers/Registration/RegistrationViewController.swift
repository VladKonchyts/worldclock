//
//  RegistrationViewController.swift
//  WorldClockApp
//
//  Created by macbook on 19.04.21.
//

import UIKit
import Firebase

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var hiddenEyeButton: UIButton!
    var isNeedToShowPassword = true

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func customBackButtonCkicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func eyeClicked(_ sender: Any) {
        passwordTextField.isSecureTextEntry.toggle()
        if isNeedToShowPassword == false {
            hiddenEyeButton.setImage(UIImage(named:"hide"), for: .normal)
            isNeedToShowPassword = true
        } else {
            hiddenEyeButton.setImage(UIImage(named:"View"), for: .normal)
            isNeedToShowPassword = false
        }
    }
    
    @IBAction func registrationButtonClicked(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
                if user != nil {
                    let worldClockVC = CustomBarViewController()
                    self.navigationController?.pushViewController(worldClockVC, animated: true)
                } else {
                    let errorMessage = error?.localizedDescription ?? "Error"
                    let alertVC = UIAlertController(title: nil, message: errorMessage, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    alertVC.addAction(action)
                    self.present(alertVC, animated: true, completion: nil)
                }
            }
        }
    }
    
}
