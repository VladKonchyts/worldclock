//
//  WorldClockViewController.swift
//  WorldClockApp
//
//  Created by macbook on 19.04.21.
//

import UIKit

class CustomBarViewController: UIViewController {
    
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var listButton: UIButton!
    @IBOutlet weak var clockButton: UIButton!
    @IBOutlet weak var viewForTab: UIView!
    
    var delegate: CustomBarDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.forHome()
        }
    }
    
    private func forHome() {
        let worldClockVC = WorldClockViewController()
        addChild(worldClockVC)
        contentView.addSubview(worldClockVC.view)
        worldClockVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        worldClockVC.didMove(toParent: self)
    }

    @IBAction func onClickedTabBar(_ sender: UIButton) {
        let tag = sender.tag
        if tag == 1 {
            listButton.setImage(UIImage(named: "ListNotActive"), for: .normal)
            clockButton.setImage(UIImage(named: "ClockIsActive"), for: .normal)
            forHome()
        } else {
            listButton.setImage(UIImage(named: "ListIsActive"), for: .normal)
            clockButton.setImage(UIImage(named: "ClockNotActive"), for: .normal)
            let clockListVC = ClockListViewController()
            addChild(clockListVC)
            contentView.addSubview(clockListVC.view)
            clockListVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            clockListVC.didMove(toParent: self)
        }
    }
    
    @IBAction func menuButtonClicked(_ sender: Any) {
        delegate?.handleMenuToggle()
        
    }
}
