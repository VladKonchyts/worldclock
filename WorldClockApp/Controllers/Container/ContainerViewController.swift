//
//  ContainerViewController.swift
//  WorldClockApp
//
//  Created by macbook on 20.04.21.
//

import UIKit

class ContainerViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    
    var menuVC: UIViewController!
    var centerVC: UIViewController!
    var isExpanded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configHomeViewController()
        
    }
    
    private func configHomeViewController() {
        let homeVC = CustomBarViewController()
        homeVC.delegate = self
        centerVC = homeVC
        addChild(centerVC)
        containerView.addSubview(centerVC.view)
        centerVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        centerVC.didMove(toParent: self)
    }
    
    private func configMenuViewController() {
        if menuVC == nil {
            menuVC = SideMenuViewController()
            addChild(menuVC)
            containerView.insertSubview(menuVC.view, at: 0)
            menuVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            menuVC.didMove(toParent: self)
            print("Did add menu controller...")
        }
    }
    
    private func showMenuVC(shouldExpand: Bool) {
        
        if shouldExpand {
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerVC.view.frame.origin.x = self.centerVC.view.frame.width - 80
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerVC.view.frame.origin.x = 0
            }, completion: nil)
        }
    }
}
 

extension ContainerViewController: CustomBarDelegate {
    func handleMenuToggle() {
        
        if !isExpanded {
            configMenuViewController()
        }
        isExpanded = !isExpanded
        showMenuVC(shouldExpand: isExpanded)
    }
}
