//
//  WorldClockViewController.swift
//  WorldClockApp
//
//  Created by macbook on 20.04.21.
//

import UIKit
import Clocket

class WorldClockViewController: UIViewController {

    
    
    @IBOutlet weak var clockView: Clocket!
    @IBOutlet weak var worldClockView: UIView!
    @IBOutlet weak var label: UILabel!
    
    var clock = DateFormatter()
    let date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clock.timeZone = .current
        clock.dateFormat = "HH:mm"
        label.text = clock.string(from: date)
        clockView.startClock()
        clockView.displayRealTime = true
    }
}
