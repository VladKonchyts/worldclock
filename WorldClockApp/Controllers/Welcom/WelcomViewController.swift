//
//  WelcomViewController.swift
//  WorldClockApp
//
//  Created by macbook on 19.04.21.
//

import UIKit

class WelcomViewController: UIViewController {

    @IBOutlet weak var createAccButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createAccButton.layer.cornerRadius = 15
        loginButton.layer.cornerRadius = 15
        navigationController?.navigationBar.isHidden = true
       
    }

    @IBAction func createAccButtonClicked(_ sender: Any) {
        let registrationVC = RegistrationViewController()
        navigationController?.pushViewController(registrationVC, animated: true)
    }
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        let loginVC = LoginViewController()
        navigationController?.pushViewController(loginVC, animated: true)
    }
}
