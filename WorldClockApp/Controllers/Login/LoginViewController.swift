//
//  LoginViewController.swift
//  WorldClockApp
//
//  Created by macbook on 19.04.21.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
    
    var isNeedToShowPassword = true

    @IBOutlet weak var hiddenEye: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                if user != nil {
                    let worldClockVC = ContainerViewController()
                    self.navigationController?.pushViewController(worldClockVC, animated: true)
                } else {
                    let errorMessage = error?.localizedDescription ?? "Error"
                    let alertVC = UIAlertController(title: nil, message: errorMessage, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    alertVC.addAction(action)
                    self.present(alertVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func eyeButtonClicked(_ sender: Any) {
        passwordTextField.isSecureTextEntry.toggle()
        if isNeedToShowPassword == false {
            hiddenEye.setImage(UIImage(named:"hide"), for: .normal)
            isNeedToShowPassword = true
        } else {
            hiddenEye.setImage(UIImage(named:"View"), for: .normal)
            isNeedToShowPassword = false
        }
    }
    
    @IBAction func customBackClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
