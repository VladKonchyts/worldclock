//
//  ClockListCell.swift
//  WorldClockApp
//
//  Created by macbook on 4.05.21.
//

import UIKit

class ClockListCell: UITableViewCell {

    @IBOutlet weak var clockLabel: UILabel!
    
    var clock = DateFormatter()
    let date = Date()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        clock.timeZone = .current
        clock.dateFormat = "HH:mm"
        clockLabel.text = clock.string(from: date)
    }

    
    
}
