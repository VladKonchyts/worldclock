//
//  ClockListViewController.swift
//  WorldClockApp
//
//  Created by macbook on 4.05.21.
//

import UIKit

class ClockListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        let xib = UINib(nibName: "ClockListCell", bundle: nil)
        tableView.register(xib, forCellReuseIdentifier: "ClockListCell")
        tableView.reloadData()

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClockListCell", for: indexPath) as! ClockListCell
        return cell
    }
}
